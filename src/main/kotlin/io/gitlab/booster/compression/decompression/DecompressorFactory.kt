package io.gitlab.booster.compression.decompression

import io.gitlab.booster.compression.CompressionAlgorithm


class DecompressorFactory {

    fun createDecompressor(compressionAlgorithm: CompressionAlgorithm): Decompressor {
        return when (compressionAlgorithm) {
            CompressionAlgorithm.DEFLATE -> DeflateDecompressor()
            CompressionAlgorithm.GZIP -> GzipDecompressor()
            CompressionAlgorithm.NONE -> NoOpDecompressor()
        }
    }
}
