package io.gitlab.booster.compression.decompression

import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.util.zip.InflaterInputStream

class DeflateDecompressor: Decompressor {

    override fun decompress(data: ByteArray): ByteArray {
        InflaterInputStream(ByteArrayInputStream(data)).use { gzipInputStream ->
            ByteArrayOutputStream().use { byteArrayOutputStream ->
                gzipInputStream.copyTo(byteArrayOutputStream)
                return byteArrayOutputStream.toByteArray()
            }
        }
    }
}
