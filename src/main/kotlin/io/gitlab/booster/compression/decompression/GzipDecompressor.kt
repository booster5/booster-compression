package io.gitlab.booster.compression.decompression

import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.util.zip.GZIPInputStream

class GzipDecompressor: Decompressor {

    override fun decompress(data: ByteArray): ByteArray {
        GZIPInputStream(ByteArrayInputStream(data)).use { gzipInputStream ->
            ByteArrayOutputStream().use { byteArrayOutputStream ->
                gzipInputStream.copyTo(byteArrayOutputStream)
                return byteArrayOutputStream.toByteArray()
            }
        }
    }
}
