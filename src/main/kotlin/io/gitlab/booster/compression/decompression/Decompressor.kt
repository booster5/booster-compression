package io.gitlab.booster.compression.decompression

interface Decompressor {

    fun decompress(data: ByteArray): ByteArray
}
