package io.gitlab.booster.compression.decompression

class NoOpDecompressor: Decompressor {

    override fun decompress(data: ByteArray): ByteArray = data
}
