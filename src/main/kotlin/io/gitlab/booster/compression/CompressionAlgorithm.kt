package io.gitlab.booster.compression

enum class CompressionAlgorithm(val algorithm: String) {
    DEFLATE("deflate"),
    GZIP("gzip"),
    NONE("identity");

    companion object {
        fun findAlgorithm(name: String): CompressionAlgorithm {
            for (value in entries) {
                if (value.algorithm == name) {
                    return value
                }
            }
            return NONE
        }
    }
}
