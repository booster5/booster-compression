package io.gitlab.booster.compression.compression

import java.io.ByteArrayOutputStream
import java.util.zip.DeflaterOutputStream

class DeflateCompressor: Compressor {
    override fun compress(data: ByteArray): ByteArray {
        val baos = ByteArrayOutputStream()
        DeflaterOutputStream(baos).use { output ->
            output.write(data)
        }
        return baos.toByteArray()
    }
}
