package io.gitlab.booster.compression.compression

class NoOpCompressor: Compressor {

    override fun compress(data: ByteArray) = data
}
