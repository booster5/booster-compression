package io.gitlab.booster.compression.compression

import java.nio.charset.StandardCharsets

/**
 * Compressor interface that compresses data
 */
interface Compressor {

    /**
     * Compresses the given data
     */
    fun compress(data: ByteArray): ByteArray

    fun compress(data: String) =
        this.compress(data.toByteArray(StandardCharsets.UTF_8))
}
