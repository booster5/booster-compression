package io.gitlab.booster.compression.compression

import io.gitlab.booster.compression.CompressionAlgorithm

class CompressorFactory {

    fun createCompressor(compressionAlgorithm: CompressionAlgorithm): Compressor {
        return when (compressionAlgorithm) {
            CompressionAlgorithm.DEFLATE -> DeflateCompressor()
            CompressionAlgorithm.GZIP -> GzipCompressor()
            CompressionAlgorithm.NONE -> NoOpCompressor()
        }
    }
}
