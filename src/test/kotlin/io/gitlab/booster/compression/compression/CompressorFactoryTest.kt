package io.gitlab.booster.compression.compression

import io.gitlab.booster.compression.CompressionAlgorithm
import io.gitlab.booster.compression.CompressionTestData
import io.gitlab.booster.compression.CompressionTestData.GZIP_COMPRESSED
import io.gitlab.booster.compression.CompressionTestData.TEXT_TO_COMPRESS
import io.gitlab.booster.compression.decompression.DecompressorFactory
import io.gitlab.booster.compression.getBase64EncodedString
import java.nio.charset.StandardCharsets
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.CoreMatchers.instanceOf
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test

class CompressorFactoryTest {

    @Test
    fun `should create compressor`() {
        for (algorithm in CompressionAlgorithm.entries) {
            val compressor = CompressorFactory().createCompressor(algorithm)
            assertThat(compressor, notNullValue())
            when (algorithm) {
                CompressionAlgorithm.NONE -> assertThat(compressor, instanceOf(NoOpCompressor::class.java))
                CompressionAlgorithm.DEFLATE -> assertThat(compressor, instanceOf(DeflateCompressor::class.java))
                CompressionAlgorithm.GZIP -> assertThat(compressor, instanceOf(GzipCompressor::class.java))
            }
        }
    }

    @Test
    fun `should compress gzip`() {
        val compressor = CompressorFactory().createCompressor(CompressionAlgorithm.GZIP)
        val compressed = compressor.compress(TEXT_TO_COMPRESS.toByteArray(StandardCharsets.UTF_8))
        assertThat(compressed, notNullValue())

        val base64 = getBase64EncodedString(compressed)
        assertThat(base64, equalTo(GZIP_COMPRESSED))

        val decompressor = DecompressorFactory().createDecompressor(CompressionAlgorithm.GZIP)
        val decompressed = decompressor.decompress(compressed).toString(StandardCharsets.UTF_8)
        assertThat(decompressed, equalTo(TEXT_TO_COMPRESS))
    }

    @Test
    fun `should compress deflate`() {
        val compressor = CompressorFactory().createCompressor(CompressionAlgorithm.DEFLATE)
        val compressed = compressor.compress(TEXT_TO_COMPRESS.toByteArray(StandardCharsets.UTF_8))
        assertThat(compressed, notNullValue())

        val base64 = getBase64EncodedString(compressed)
        assertThat(base64, equalTo(CompressionTestData.DEFLATE_COMPRESSED))

        val decompressor = DecompressorFactory().createDecompressor(CompressionAlgorithm.DEFLATE)
        val decompressed = decompressor.decompress(compressed).toString(StandardCharsets.UTF_8)
        assertThat(decompressed, equalTo(TEXT_TO_COMPRESS))
    }

    @Test
    fun `should compress noop`() {
        val compressor = CompressorFactory().createCompressor(CompressionAlgorithm.NONE)
        val compressed = compressor.compress(TEXT_TO_COMPRESS.toByteArray(StandardCharsets.UTF_8))
        assertThat(compressed, notNullValue())

        val base64 = getBase64EncodedString(compressed)
        assertThat(base64, equalTo(getBase64EncodedString(TEXT_TO_COMPRESS.toByteArray(StandardCharsets.UTF_8))))

        val decompressor = DecompressorFactory().createDecompressor(CompressionAlgorithm.NONE)
        val decompressed = decompressor.decompress(compressed).toString(StandardCharsets.UTF_8)
        assertThat(decompressed, equalTo(TEXT_TO_COMPRESS))
    }
}
