package io.gitlab.booster.compression.decompression

import io.gitlab.booster.compression.CompressionAlgorithm
import org.hamcrest.CoreMatchers.instanceOf
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test


class DecompressorFactoryTest {

    @Test
    fun `should create decompressor`() {
        for (compressionAlgorithm in CompressionAlgorithm.entries) {
            val decompressor = DecompressorFactory().createDecompressor(compressionAlgorithm)
            when (compressionAlgorithm) {
                CompressionAlgorithm.NONE -> assertThat(decompressor, instanceOf(NoOpDecompressor::class.java))
                CompressionAlgorithm.DEFLATE -> assertThat(decompressor, instanceOf(DeflateDecompressor::class.java))
                CompressionAlgorithm.GZIP -> assertThat(decompressor, instanceOf(GzipDecompressor::class.java))
            }
        }
    }
}
