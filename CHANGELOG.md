# Change Logs

## v3.2.0
1. Update to Spring Boot 3.4.2
2. Update to Spring Cloud 2024.0.0
3. Update to Spring Cloud GCP 6.0.0
4. Update to JVM 21
5. Update dependencies

## v3.1.0
1. Update to Spring Boot 3.3.7
2. Update to Spring Cloud 2023.0.4
3. Update to Spring Cloud GCP 5.9.0

## v3.0.1
1. Add ZSTD compression.

## v3.0.0
1. Update to Spring Boot 3

## v1.0.0
1. Initial version
2. Change of strategy, import base only as dependencyManagement.
